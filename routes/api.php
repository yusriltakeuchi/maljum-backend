<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\StoryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'auth'], function() {
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);    
});

Route::group([
    'prefix' => 'category',
    'middleware' => 'auth:api'
], function() {
    Route::get('/', [CategoryController::class, 'getAll']);
});

Route::group([
    'prefix' => 'story',
    'middleware' => 'auth:api'
], function() {
    Route::get('/latest', [StoryController::class, 'getLatest']);
    Route::get('/popular', [StoryController::class, 'getPopular']);
    Route::get('/mostliked', [StoryController::class, 'getMostLiked']);
    Route::get('/detail/{conversation_id}', [StoryController::class, 'getDetail']);
    Route::get('/content/{conversation_id}', [StoryController::class, 'getContent']);
    Route::get('/latest_read', [StoryController::class, 'getLatestRead']);
    Route::get('/related/{category_id}', [StoryController::class, 'related']);
});