<?php

namespace App\Services;

use App\Models\Category;

class CategoryServices {
    private $categoryModel;
    public function __construct()
    {
        $this->categoryModel = new Category();
    }

    public function getAll()
    {
        return $this->categoryModel->get();
    }
}