<?php

namespace App\Services;

use App\Models\Story;

class StoryServices {
    private $storyModel;
    public function __construct()
    {
        $this->storyModel = new Story();
    }

    public function getLatest()
    {
        return $this->storyModel
            ->with('category')
            ->orderBy('created_at', 'DESC')
            ->paginate(20);
    }

    public function getPopular()
    {
        return $this->storyModel
            ->with('category')
            ->orderBy('view_count', 'DESC')
            ->paginate(20);
    }

    public function getMostLiked()
    {
        return $this->storyModel
            ->with('category')
            ->orderBy('like_count', 'DESC')
            ->paginate(20);
    }

    public function getRelated($categoryId)
    {
        return $this->storyModel
            ->with('category')
            ->where('category_id', $categoryId)
            ->paginate(20);
    }

    public function getDetail($conversation_id)
    {
        return $this->storyModel
            ->with('category')
            ->where('conversation_id', $conversation_id)
            ->first();
    }

    public function getById($id)
    {
        return $this->storyModel
            ->with('category')
            ->find($id);
    }

    public function getContent($conversation_id)
    {
        return $this->storyModel
            ->select('conversation_id','contents')
            ->where('conversation_id', $conversation_id)
            ->first();
    }
}