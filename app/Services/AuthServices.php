<?php

namespace App\Services;

use App\Helpers\LoggingHelper;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;

class AuthServices {
    private $userModel;
    private $logging;
    public function __construct()
    {
        $this->userModel = new User();
        $this->logging = new LoggingHelper();
    }

    public function register($request)
    {
        DB::beginTransaction();
        try {
            $user = User::create([
                'username' => $request->username,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);
            if ($user) {
                $profile = $user->profile()->create($request->all());
                if ($profile) {
                    DB::commit();
                    return true;
                }
            }
            return false;
        } catch(Exception $e) {
            $this->logging->error($e);
            DB::rollBack();
        }
        return false;
    }

    public function isEmailExists($email)
    {
        return $this->userModel->isEmailExists($email);
    }

    public function isUsernameExists($username)
    {
        return $this->userModel->isUsernameExists($username);
    }
}