<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Http\Resources\StoryPageCollection;
use App\Http\Resources\StoryResource;
use App\Services\StoryServices;
use Illuminate\Http\Request;
use Story;

class StoryController extends Controller
{
    private $response;
    private $storyServices;

    public function __construct(ResponseHandler $response, StoryServices $storyServices)
    {
        $this->response = $response;
        $this->storyServices = $storyServices;
    }


    public function getLatest()
    {
        $story = $this->storyServices->getLatest();
        if ($story) {
            return $this->response->send(200, "Berhasil mendapatkan cerita", new StoryPageCollection($story));
        }
        return $this->response->notFound("Cerita");
    }

    public function getPopular()
    {
        $story = $this->storyServices->getPopular();
        if ($story) {
            return $this->response->send(200, "Berhasil mendapatkan cerita", new StoryPageCollection($story));
        }
        return $this->response->notFound("Cerita");
    }

    public function getMostLiked()
    {
        $story = $this->storyServices->getMostLiked();
        if ($story) {
            return $this->response->send(200, "Berhasil mendapatkan cerita", new StoryPageCollection($story));
        }
        return $this->response->notFound("Cerita");
    }

    public function getDetail($conversation_id)
    {
        if (!isset($conversation_id)) {
            return $this->response->badRequest("Conversation id tidak valid");
        }

        $story = $this->storyServices->getDetail($conversation_id);
        if ($story) {
            return $this->response->send(200, "Berhasil mendapatkan cerita", new StoryResource($story));
        }
        return $this->response->notFound("Cerita");
    }

    public function getLatestRead(Request $request)
    {
        $user = $request->user();

        $story = $this->storyServices->getById($user->story_id);
        if ($story) {
            return $this->response->send(200, "Berhasil mendapatkan cerita", new StoryResource($story));
        }
        return $this->response->notFound("Cerita");
    }

    public function getContent($conversation_id)
    {
        if (!isset($conversation_id)) {
            return $this->response->badRequest("Conversation id tidak valid");
        }

        $story = $this->storyServices->getContent($conversation_id);
        if ($story) {
            return $this->response->send(200, "Berhasil mendapatkan cerita", $story);
        }
        return $this->response->notFound("Cerita");
    }

    public function related($categoryId)
    {
        if (!isset($categoryId)) {
            return $this->response->badRequest("Kategori id tidak valid");
        }

        $story = $this->storyServices->getRelated($categoryId);
        if ($story) {
            return $this->response->send(200, "Berhasil mendapatkan cerita", new StoryPageCollection($story));
        }
        return $this->response->notFound("Cerita");
    }

}
