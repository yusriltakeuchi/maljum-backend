<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use App\Services\CategoryServices;

class CategoryController extends Controller
{
    private $response;
    private $categoryServices;

    public function __construct(ResponseHandler $response, CategoryServices $categoryServices)
    {
        $this->response = $response;
        $this->categoryServices = $categoryServices;
    }

    public function getAll()
    {
        $category = $this->categoryServices->getAll();
        if ($category && $category->count() > 0) {
            return $this->response->send(200, "Berhasil mendapatkan kategori", $category);
        }
        return $this->response->notFound("Kategori");
    }
}
