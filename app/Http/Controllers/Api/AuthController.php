<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ResponseHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\AuthServices;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    private $response;
    private $authServices;

    public function __construct(ResponseHandler $response, AuthServices $authServices)
    {
        $this->response = $response;
        $this->authServices = $authServices;
    }
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required',
            'name' => 'required',
            'phone' => 'required|min:10|unique:profile'
        ]);

        /// Validating request input
        if ($validator->fails()) {
            return $this->response->validateError($validator->errors(), true);
        }

        $result = $this->authServices->register($request);
        if ($result) {
            return $this->response->send(200, "Berhasil melakukan pendaftaran akun");
        } else {
            return $this->response->badRequest("Gagal melakukan pendaftaran akun");
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        /// Validating request input
        if ($validator->fails()) {
            return $this->response->validateError($validator->errors(), true);
        }

        /// Validate email and password correct
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials)){
            return $this->response->badCredentials();
        }

        $token = Auth()->user()->createToken('authToken')->accessToken;
        return $this->response->send(200, "Berhasil melakukan login", [
            'access_token' => $token
        ]);
    }
}
