<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'conversation_id' => $this->conversation_id,
            'title' => $this->title,
            'contents' => join("\n\n", array_slice(
                explode("\n\n", $this->contents), 0, 3)
            ),
            'author' => $this->author,
            'image' => $this->image,
            'created_at' => $this->created_at,
            'category' => $this->category
        ];
    }
}
