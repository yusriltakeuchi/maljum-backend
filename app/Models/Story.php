<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    use HasFactory;
    public $table = "story";
    protected $fillable = [
        'conversation_id',
        'title',
        'contents',
        'author',
        'image',
        'verified',
        'like_count',
        'comment_count',
        'view_count',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
