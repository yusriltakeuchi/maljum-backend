<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;

class LoggingHelper {

    public function error($message)
    {
        $dateNow = date("Y-m-d H:i:s");
        $msgPayload = "[$dateNow][ERROR] - $message";
        Log::channel("error")->info($msgPayload);
    }
}