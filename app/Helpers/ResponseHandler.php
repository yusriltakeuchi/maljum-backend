<?php

namespace App\Helpers;

class ResponseHandler
{
    public function send($status = 200, $message, $data = [])
    {
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], $status);
    }

    public function badRequest($msg)
    {
        return response()->json([
            'status' => 400,
            'message' => $msg,
        ], 400);
    }

    public function notFound($message)
    {
        return response()->json([
            'status' => 404,
            'message' => "$message tidak ditemukan",
        ], 404);
    }

    public function badCredentials()
    {
        return response()->json([
            'status' => 401,
            'message' => "Email atau password salah",
        ], 401);
    }

    public function badOldPassword()
    {
        return response()->json([
            'status' => 400,
            'message' => "Password lama tidak sama",
        ], 400);
    }

    public function internalError()
    {
        return response()->json([
            'status' => 500,
            'message' => "Internal server error",
        ], 500);
    }

    public function exists($message)
    {
        return response()->json([
            'status' => 400,
            'message' => "$message sudah ada",
        ], 400);
    }

    public function validateError($errors, $fromValidate = true)
    {
        return response()->json([
            'status' => 422,
            'message' => $fromValidate ? $errors->first() : $errors,
        ], 422);
    }
}
